﻿using System;

namespace Affine_Cipher
{
    class CipherTools
    {
        // Bellow ciphering - only lower case
        public static string Ciphering(string text, int a, int b)
        {
            string Cipher = string.Empty;
            for (int i = 0; i < text.Length; i++)
            {
                int x = ((a * ((text[i]) - 97) + b) % 26);
                while( x > 26)
                { 
                    x -= 26;
                }
                x += 97;
                Cipher += (char)x;
            }
            return Cipher;
        }
        public static string Deciphering(string cipherText, int a, int b)
        {
            int x, z = 0 ;
            string output = string.Empty;
            for (int i = 0; i < 26; i++)
            {
                x = (a * i) % 26;
                if(x == 1)
                {
                    z = i;
                }

            }
            for (int i = 0; i < cipherText.Length; i++)
            {
                x = ((z * ((cipherText[i] - 97) - b)) % 26);
                output += (char) (x + 97 );
              //  Console.WriteLine(x + 97);
            }

            return output;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            string text = "aabbccddeeffgghhiijjkkllmmmn";
            int a = 7;
            int b = 5;
            string cipher = CipherTools.Ciphering(text, a, b);

            Console.WriteLine(text);
            Console.WriteLine(cipher);
            Console.WriteLine(CipherTools.Deciphering(cipher,a,b));
        }
    }
}
